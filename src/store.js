import Vue from 'vue';
import Vuex from 'vuex';
import auth from './store/modules/auth/index';
import employee from './store/modules/employee/index';
import admin from './store/modules/admin/index';
Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    employee,
    admin
  }
});
