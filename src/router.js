import Vue from 'vue';
import Router from 'vue-router';
import Login from './views/auth/Login.vue';
import Home from './views/employee/Home.vue';
import Calendar from './views/employee/Calendar.vue';
import History from './views/employee/History.vue';
import Dashboard from './views/admin/Dashboard.vue';
import Departments from './views/admin/Departments.vue';
import DateTime from './views/admin/DateTime.vue';
import Bookings from './views/admin/Bookings.vue';
import store from "./store.js";
import NProgress from "nprogress";

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'index',
      component: Login,
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/home',
      name: 'home',
      component: Home,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/calendar',
      name: 'calendar',
      component: Calendar,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/history',
      name: 'history',
      component: History,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
      meta: {
        requiresAdmin: true
      }
    },
    {
      path: '/departments',
      name: 'departments',
      component: Departments,
      meta: {
        requiresAdmin: true
      }
    },
    {
      path: '/date-time',
      name: 'dateTime',
      component: DateTime,
      meta: {
        requiresAdmin: true
      }
    },
    {
      path: '/bookings',
      name: 'bookings',
      component: Bookings,
      meta: {
        requiresAdmin: true
      }
    }
  ],
});
router.beforeEach((to, from, next) => {
  if (to.name) {
    // Start the route progress bar.
    NProgress.start();
  }
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn && store.getters.getRole == 'employee') {
      next();
      return;
    }
    next("/");
  } else if (to.matched.some(record => record.meta.requiresAdmin)) {
    if (store.getters.getRole == 'admin' && store.getters.isLoggedIn) {
      next();
      return;
    }
    next("/");
  } else {
    next();
  }
});
router.afterEach((to, from) => {
  // Complete the animation of the route progress bar.
  NProgress.done();
});
export default router;