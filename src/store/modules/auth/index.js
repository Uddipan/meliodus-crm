import actions from "./actions";
import mutations from "./mutations";
import getters from "./getters";

const state = {
  status: "",
  token: localStorage.getItem("token") || "",
  role:localStorage.getItem("role") || ""
};

const auth = {
  state,
  actions,
  mutations,
  getters
};
export default auth;
