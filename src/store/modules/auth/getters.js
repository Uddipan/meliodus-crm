const getters = {
  isLoggedIn: state => !!state.token,
  getRole: state => state.role
};
  
export default getters;
  