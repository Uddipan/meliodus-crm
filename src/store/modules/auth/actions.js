import axios from "axios";

export default {
  login({ commit }, user) {
    return new Promise((resolve, reject) => {
      axios({
        url: "auth/login",
        data: user,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        method: "POST",
      }).then(resp => {
        const token = resp.data.token;
        const role = resp.data.role;
        localStorage.setItem("token", token);
        localStorage.setItem("role", role);
        axios.defaults.headers.common["Authorization"] = token;
        commit("auth_success", { token, role });
        resolve(resp);
      }).catch(err => {
        localStorage.removeItem("token");
        localStorage.removeItem("role");
        reject(err);
      })
    })
  },
  logout({ commit }) {
    return new Promise((resolve, reject) => {
      commit("logout");
      localStorage.removeItem("token");
      localStorage.removeItem("role");
      delete axios.defaults.headers.common["Authorization"];
      resolve();
    });
  },
}