const mutations = {
    auth_success(state, {token, role} ) {
        state.token = token;
        state.role = role;
    },
    logout(state) {
        state.status = "";
        state.token = "";
        state.role = "";
    }
};
export default mutations;