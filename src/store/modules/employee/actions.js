import axios from "axios";

export default {
  fetchProfile({ commit }) {
    return new Promise((resolve, reject) => {
      axios({
        url: "user",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        method: "GET",
      }).then(resp => {
        const user = resp.data;
        commit("setProfile", { user });
        resolve(resp);
      }).catch(err => {
        reject(err);
      })
    })
  },
  fetchDepartments({ commit }) {
    return new Promise((resolve, reject) => {
      axios({
        url: "department",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        method: "GET",
      }).then(resp => {
        const departments = resp.data;
        commit("setDepartment", { departments });
        resolve(resp);
      }).catch(err => {
        reject(err);
      })
    })
  }
}