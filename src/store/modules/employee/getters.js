const getters = {
    profile: state => state.user,
    booking: state => state.bookingData,
    departments: state => state.departments.dept_info
  };
  
export default getters;
  