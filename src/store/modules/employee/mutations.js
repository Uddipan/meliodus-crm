const mutations = {
    setProfile(state, {user} ) {
        state.user = user;
    },
    setDepartment(state, {departments}){
        state.departments = departments
    }
};
export default mutations;