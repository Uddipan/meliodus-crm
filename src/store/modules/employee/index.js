import actions from "./actions";
import mutations from "./mutations";
import getters from "./getters";

const state = {
  user: {},
  bookingData:{},
  departments:{}
};

const auth = {
  state,
  actions,
  mutations,
  getters
};
export default auth;
