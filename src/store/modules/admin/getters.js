const getters = {
    users: state => state.users.user_info,
    bookings: state => state.bookings,
    showLoadMore: state => state.showLoadMore,
    weekends: state => state.weekends,
    holidays: state => state.holidays,
    officeHours: state => state.officeHours,
    intervals: state => state.intervals,
  };
  
export default getters;
  