import axios from "axios";

export default {
	fetchUsers({ commit }) {
		return new Promise((resolve, reject) => {
			axios({
				url: "user/show",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				method: "GET",
			}).then(resp => {
				const users = resp.data;
				commit("setUsers", { users });
				resolve(resp);
			}).catch(err => {
				reject(err);
			})
		})
	},
	addUser({ commit }, userData) {
		return new Promise((resolve, reject) => {
			axios({
				url: "user/create",
				data: userData,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				method: "POST",
			}).then(resp => {
				resolve(resp);
			}).catch(err => {
				reject(err);
			})
		})
	},
	deleteUser({ commit }, formData) {
		return new Promise((resolve, reject) => {
			axios({
				url: "user/delete",
				data: formData,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				method: "POST",
			}).then(resp => {
				resolve(resp);
			}).catch(err => {
				reject(err);
			})
		})
	},
	editUser({ commit }, formData) {
		return new Promise((resolve, reject) => {
			axios({
				url: "user/update",
				data: formData,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				method: "POST",
			}).then(resp => {
				resolve(resp);
			}).catch(err => {
				reject(err);
			})
		})
	},
	addDepartment({ commit }, deptData) {
		return new Promise((resolve, reject) => {
			axios({
				url: "department/create",
				data: deptData,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				method: "POST",
			}).then(resp => {
				resolve(resp);
			}).catch(err => {
				reject(err);
			})
		})
	},
	deleteDepartment({ commit }, formData) {
		return new Promise((resolve, reject) => {
			axios({
				url: "department/delete",
				data: formData,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				method: "POST",
			}).then(resp => {
				resolve(resp);
			}).catch(err => {
				reject(err);
			})
		})
	},
	editDepartment({ commit }, formData) {
		return new Promise((resolve, reject) => {
			axios({
				url: "department/update",
				data: formData,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				method: "POST",
			}).then(resp => {
				resolve(resp);
			}).catch(err => {
				reject(err);
			})
		})
	},
	addBooking({ commit }, formData) {
		return new Promise((resolve, reject) => {
			axios({
				url: "bookings/create",
				data: formData,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				method: "POST",
			}).then(resp => {
				commit("addBooking");
				resolve(resp);
			}).catch(err => {
				reject(err);
			})
		})
	},
	fetchBookings({ commit }, data) {
		return new Promise((resolve, reject) => {
			let url = "";
			if (data)
				url = "bookings/" + data.limit + "/" + data.offset
			else
				url = "bookings"
			axios({
				url,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				method: "GET",
			}).then(resp => {
				const bookings = resp.data;
				if (bookings.bookings.length)
					commit("setBookings", { bookings });
				else
					commit("changeLoadMore", false)
				resolve(resp);
			}).catch(err => {
				reject(err);
			})
		})
	},
	changeBookingStatus({ commit }, formData) {
		return new Promise((resolve, reject) => {
			axios({
				url: "bookings/change-status",
				data: formData,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				method: "POST",
			}).then(resp => {
				resolve(resp);
			}).catch(err => {
				reject(err);
			})
		})
	},
	fetchWeekends({ commit }) {
		return new Promise((resolve, reject) => {
			axios({
				url: "weekend",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				method: "GET",
			}).then(resp => {
				const weekends = resp.data;
				commit("setWeekends", weekends)
				resolve(resp);
			}).catch(err => {
				reject(err);
			})
		})
	},
	addWeekend({ commit }, formData) {
		return new Promise((resolve, reject) => {
			axios({
				url: "weekend/create",
				data: formData,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				method: "POST",
			}).then(resp => {
				resolve(resp);
			}).catch(err => {
				reject(err);
			})
		})
	},
	deleteWeekend({ commit }, formData) {
		return new Promise((resolve, reject) => {
			axios({
				url: "weekend/delete",
				data: formData,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				method: "POST",
			}).then(resp => {
				resolve(resp);
			}).catch(err => {
				reject(err);
			})
		})
	},
	fetchHolidays({ commit }) {
		return new Promise((resolve, reject) => {
			axios({
				url: "holiday",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				method: "GET",
			}).then(resp => {
				const holidays = resp.data;
				commit("setHolidays", holidays)
				resolve(resp);
			}).catch(err => {
				reject(err);
			})
		})
	},
	addHoliday({ commit }, formData) {
		return new Promise((resolve, reject) => {
			axios({
				url: "holiday/create",
				data: formData,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				method: "POST",
			}).then(resp => {
				resolve(resp);
			}).catch(err => {
				reject(err);
			})
		})
	},
	deleteHoliday({ commit }, formData) {
		return new Promise((resolve, reject) => {
			axios({
				url: "holiday/delete",
				data: formData,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				method: "POST",
			}).then(resp => {
				resolve(resp);
			}).catch(err => {
				reject(err);
			})
		})
	},
	addInterval({ commit }, formData) {
		return new Promise((resolve, reject) => {
			axios({
				url: "interval/create",
				data: formData,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				method: "POST",
			}).then(resp => {
				resolve(resp);
			}).catch(err => {
				reject(err);
			})
		})
	},
	fetchOfficeHours({ commit }) {
		return new Promise((resolve, reject) => {
			axios({
				url: "office-hours",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				method: "GET",
			}).then(resp => {
				const officeHours = resp.data;
				commit("setOfficeHours", officeHours)
				resolve(resp);
			}).catch(err => {
				reject(err);
			})
		})
	},
	addOfficeHours({ commit }, formData) {
		return new Promise((resolve, reject) => {
			axios({
				url: "office-hours/create",
				data: formData,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				method: "POST",
			}).then(resp => {
				resolve(resp);
			}).catch(err => {
				reject(err);
			})
		})
	},
	fetchIntervals({ commit }) {
		return new Promise((resolve, reject) => {
			axios({
				url: "interval",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				method: "GET",
			}).then(resp => {
				const intervals = resp.data;
				commit("setIntervals", intervals)
				resolve(resp);
			}).catch(err => {
				reject(err);
			})
		})
	},
}