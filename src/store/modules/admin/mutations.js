const mutations = {
    setUsers(state, { users }) {
        state.users = users;
    },
    addBooking(state) {
      localStorage.removeItem("mode_of_booking");
      localStorage.removeItem("dept_id");
    },
    setBookings(state, { bookings }) {
        state.bookings = [...state.bookings,...bookings.bookings];
    },
    changeLoadMore(state, status) {
        state.showLoadMore = status
    },
    setWeekends(state, weekends) {
        state.weekends = weekends.weekends
    },
    
    setHolidays(state, holidays) {
        state.holidays = holidays.holidays
    },
    setOfficeHours(state, officeHours) {
        state.officeHours = officeHours.officeHours
    },
    setIntervals(state, intervals) {
        state.intervals = intervals.intervals
    },
};
export default mutations;