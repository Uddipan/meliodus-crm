import actions from "./actions";
import mutations from "./mutations";
import getters from "./getters";

const state = {
  users: {},
  bookings:[],
  showLoadMore: true,
  weekends:[],
  holidays:[],
  officeHours:[],
  intervals:[]
};

const auth = {
  state,
  actions,
  mutations,
  getters
};
export default auth;
